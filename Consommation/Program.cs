﻿using System;
using DAL.Entities;
using DAL.Services;
using System.Collections.Generic;

namespace Consommation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //On créer une liste d'étudiants vide
            List<Student> I3GameStudent;
            //On créer une nouvelle instance de StudentService
            StudentService Service = new StudentService();

            I3GameStudent = Service.GetAll();
            foreach(Student Student in I3GameStudent)
            {
                Console.WriteLine($"Je vous présente {Student.FirstName} {Student.LastName} et son jeu favori de tous les temps est : {Student.FavGame}");
            }

            //Affichage du nombre d'étudiantes
            Console.WriteLine($"Il y a {Service.GetStudentCount()} étudiantes dans le groupe I3Games2022");

            //Création et ajout d'une étudiante
            Student Aude = new Student();
            Aude.LastName = "Aude";
            Aude.FirstName = "Beurive";
            Aude.FavGame = "Dark Cloud";
            Service.Add(Aude);

            //Récupération et affichage de la liste après insertion
            I3GameStudent = Service.GetAll();
            foreach (Student Student in I3GameStudent)
            {
                Console.WriteLine($"Je vous présente {Student.FirstName} {Student.LastName} et son jeu favori de tous les temps est : {Student.FavGame}");
            }

            //Récupération d'une étudiante en particulier
            Student EtudianteRecuperee = Service.GetById("3");
            //Student EtudianteRecuperee = Service.GetById("3; TRUNCATE TABLE Student");
            Console.WriteLine($"Etudiante récupérée : {EtudianteRecuperee.LastName} {EtudianteRecuperee.FirstName}");

            Console.WriteLine("Affichage de toutes les étudiantes :");
            I3GameStudent = Service.GetAll();
            foreach (Student Student in I3GameStudent)
            {
                Console.WriteLine($"Je vous présente {Student.FirstName} {Student.LastName} et son jeu favori de tous les temps est : {Student.FavGame}");
            }
            Console.ReadLine();
        }
    }
}
