﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAL.Services
{
    public class StudentService
    {
        private string _connectionString = "server=(localdb)\\games;database=I3Games;integrated security=true";
        public List<Student> GetAll()
        {
            List<Student> ListARenvoyer = new List<Student>();
            //Remplir la liste
            //using (instance)
            //{

            //}//instance est détruit
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                //On ouvre la connection à la base de données
                connection.Open();
                //On créer une nouvelle commande SQL à partir de la connection
                SqlCommand commande = connection.CreateCommand();
                //On modifie le texte de notre commande
                commande.CommandText = "SELECT * FROM Student";
                //ExecuteScalar -> Utilisé pour les requêtes qui renvoient un seul résultat (un prénom, un nombre etc) renvoie le résultat de la requête
                //commande.ExecuteScalar()
                //ExecuteNonQuery -> Utilisé pour tout ce qui n'est pas une requête (insertion, suppression, modification), elle renverra le nombre de lignes affectées
                //commande.ExecuteNonQuery()
                //ExecuteReader -> Utilisé pour les requêtes qui renvoient un ensemble de données (une entité entière, une table, etc)
                //elle renvoie un objet de type SqlDataReader
                SqlDataReader reader = commande.ExecuteReader();
                //La méthode Read() lit la prochaine entrée récupérée, elle renvoie true si elle a trouvé une ligne et false si elle n'a rien trouvé
                while (reader.Read())
                {
                    Student StudentToAdd = new Student();
                    //On met dans la propriété Id de notre objet Student
                    //La valeur qu'il y a pour la colonne Id
                    StudentToAdd.Id = (int)reader["Id"];
                    StudentToAdd.FirstName = (string)reader["FirstName"];
                    StudentToAdd.LastName = (string)reader["LastName"];
                    StudentToAdd.FavGame = (string)reader["FavGame"];
                    ListARenvoyer.Add(StudentToAdd);
                }
            }
            //Renvoie la liste avec tous les Students
            return ListARenvoyer;
        }

        public Student GetById(string Id)
        {
            // Student StudentARenvoyer = new Student();
            Student StudentARenvoyer;
            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand commande = connection.CreateCommand();
                commande.CommandText = $"SELECT * FROM Student WHERE Id = {Id}";
                //commande.CommandText = $"SELECT * FROM Student WHERE Id = 3";
                SqlDataReader reader = commande.ExecuteReader();
                reader.Read();

                StudentARenvoyer = new Student()
                {
                    Id = (int)reader["Id"],
                    FirstName = (string)reader["FirstName"],
                    LastName = (string)reader["LastName"],
                    FavGame = (string)reader["FavGame"],
                };
                //StudentARenvoyer.Id = (int)reader["Id"];
                //StudentARenvoyer.FirstName = (string)reader["FirstName"];
                //StudentARenvoyer.LastName = (string)reader["LastName"];
                //StudentARenvoyer.FavGame = (string)reader["FavGame"];

            }
            return StudentARenvoyer;
        }


        public int GetStudentCount()
        {
            int count = 0;

            using(SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT COUNT(*) FROM Student";

                count = (int)command.ExecuteScalar();
            }

            return count;
        }

        public void Add(Student Student)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO Student(LastName, FirstName, FavGame) VALUES ('{Student.LastName}', '{Student.FirstName}', '{Student.FavGame}')";

                command.ExecuteNonQuery();
                
            }
        }
    }
}
