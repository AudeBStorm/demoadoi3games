﻿/*
Script de déploiement pour I3Games

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "I3Games"
:setvar DefaultFilePrefix "I3Games"
:setvar DefaultDataPath "C:\Users\BSTORMAude\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\Games\"
:setvar DefaultLogPath "C:\Users\BSTORMAude\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\Games\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Création de Table [dbo].[Student]...';


GO
CREATE TABLE [dbo].[Student] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [FirstName] NVARCHAR (50)  NOT NULL,
    [LastName]  NVARCHAR (50)  NOT NULL,
    [FavGame]   NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
use I3Games


Set Identity_insert Student on;
INSERT INTO student (ID, Lastname, Firstname, FavGame) VALUES
(1, N'Bouvier', N'Virginie', N'My Little Nightmare'),
(2, N'Coeurnelle', N'Vanessa', N'Undertale'),
(3, N'Donck', N'Julie', N'Metal Gear Rising'),
(4, N'Drame', N'Naïké', N'Final Fantasy IX'),
(5, N'Levchenko', N'Alina', N'Les Sims'),
(6, N'Mahaux', N'Shaila', N'It takes two'),
(7, N'Quinta', N'Resa', N'Life is Strange'),
(8, N'Zourhbat', N'Scheherazad', N'Super Mario Land'),
(9, N'Dalcq', N'Charlotte', N'Resident Evil IV'),
(10, N'Paulus', N'Manon', N'Resident Evil I'),
(11, N'Dahdouh', N'Zineb', N'League of Legends')

Set Identity_insert Student off;

GO

GO
PRINT N'Mise à jour terminée.';


GO
