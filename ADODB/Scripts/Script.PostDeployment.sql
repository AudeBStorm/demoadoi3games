﻿use I3Games


Set Identity_insert Student on;
INSERT INTO student (ID, Lastname, Firstname, FavGame) VALUES
(1, N'Bouvier', N'Virginie', N'My Little Nightmare'),
(2, N'Coeurnelle', N'Vanessa', N'Undertale'),
(3, N'Donck', N'Julie', N'Metal Gear Rising'),
(4, N'Drame', N'Naïké', N'Final Fantasy IX'),
(5, N'Levchenko', N'Alina', N'Les Sims'),
(6, N'Mahaux', N'Shaila', N'It takes two'),
(7, N'Quinta', N'Resa', N'Life is Strange'),
(8, N'Zourhbat', N'Scheherazad', N'Super Mario Land'),
(9, N'Dalcq', N'Charlotte', N'Resident Evil IV'),
(10, N'Paulus', N'Manon', N'Resident Evil I'),
(11, N'Dahdouh', N'Zineb', N'League of Legends')

Set Identity_insert Student off;

