﻿CREATE TABLE [dbo].[Student]
(
	[Id] INT NOT NULL IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [FavGame] NVARCHAR(250) NOT NULL,
    CONSTRAINT [PK_Student] PRIMARY KEY ([Id]), 
)

GO
